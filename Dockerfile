FROM alpine:latest

RUN apk add --no-cache python3 gettext && \
    pip3 install --upgrade pip setuptools httpie && \
    rm -r /root/.cache

CMD ["/bin/sh"]